<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="26"/>
        <source>Garuda Network Assistant</source>
        <translation>Garuda Sieťový Asistent</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="70"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="76"/>
        <source>IP address</source>
        <translation>IP adresa</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="112"/>
        <source>Hardware detected</source>
        <translation>Nájdený Hardvér&#xa0;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="143"/>
        <location filename="../mainwindow.ui" line="389"/>
        <location filename="../mainwindow.ui" line="502"/>
        <source>Re-scan</source>
        <translation>Vzhľadať znova</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="215"/>
        <source>Active interface</source>
        <translation>Aktívny interface</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="231"/>
        <source>WiFi status</source>
        <translation>Stav Wifi siete</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="253"/>
        <source>Unblocks all soft/hard blocked wireless devices</source>
        <translation>Odblokuje všetky blokované bezdrátové zariadenia</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="256"/>
        <source>Unblock WiFi Devices</source>
        <translation>Odblokovať WiFi zariadenia</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="288"/>
        <source>Linux drivers</source>
        <translation>Ovládače Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="300"/>
        <source>Associated Linux drivers</source>
        <translation>Priradené Linuxové ovládače </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="309"/>
        <source>Load Driver</source>
        <translation>Načítať ovládač</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="372"/>
        <source>Unload Driver</source>
        <translation>Uvoľniť ovládač</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="409"/>
        <source>Blacklist Driver</source>
        <translation>Pridať ovládač na zoznam zakázaných</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="428"/>
        <source>Windows drivers</source>
        <translation>Ovládače Windows</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="434"/>
        <source>Available Windows drivers</source>
        <translation>Dostupné ovládače Windows</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="468"/>
        <source>Remove Driver</source>
        <translation>Vymazať ovládač</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="479"/>
        <source>Add Driver</source>
        <translation>Pridať ovládač</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="520"/>
        <source>About NDISwrapper</source>
        <translation>O programe NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="532"/>
        <source>Install NDISwrapper</source>
        <translation>Inštalovať NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="543"/>
        <source>In order to use Windows drivers you need first to install NDISwrapper</source>
        <translation>Ak chcete použiť ovládače Windows, musíte najskôr nainštalovať NDISwrapper.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="559"/>
        <source>Uninstall NDISwrapper</source>
        <translation>Odinštalovať NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="591"/>
        <source>Net diagnostics</source>
        <translation>Diagnostika siete</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="603"/>
        <source>Ping</source>
        <translation>Ping</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="609"/>
        <location filename="../mainwindow.ui" line="725"/>
        <source>Target URL:</source>
        <translation>Cieľová URL:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="619"/>
        <source>Packets</source>
        <translation>Pakety</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="661"/>
        <location filename="../mainwindow.ui" line="780"/>
        <source>Start</source>
        <translation>Štart</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="678"/>
        <location filename="../mainwindow.ui" line="797"/>
        <source>Clear</source>
        <translation>Vyčistiť</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="692"/>
        <location filename="../mainwindow.ui" line="811"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="719"/>
        <source>Traceroute</source>
        <translation>Traceroute</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="735"/>
        <source>Hops</source>
        <translation>Hops</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="866"/>
        <source>About...</source>
        <translation>O Programe</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="889"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="957"/>
        <source>&amp;Close</source>
        <translation>&amp;Zatvoriť</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="964"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>IP address from router:</source>
        <translation>IP adresa z routra:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="88"/>
        <source>External IP address:</source>
        <translation>Externá IP adresa:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="211"/>
        <location filename="../mainwindow.cpp" line="225"/>
        <location filename="../mainwindow.cpp" line="239"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopírovať</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="213"/>
        <location filename="../mainwindow.cpp" line="227"/>
        <location filename="../mainwindow.cpp" line="241"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="214"/>
        <location filename="../mainwindow.cpp" line="228"/>
        <location filename="../mainwindow.cpp" line="242"/>
        <source>Copy &amp;All</source>
        <translation>Kopírovať &amp;všetko</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="216"/>
        <location filename="../mainwindow.cpp" line="230"/>
        <location filename="../mainwindow.cpp" line="244"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="281"/>
        <location filename="../mainwindow.cpp" line="303"/>
        <source>Traceroute not installed</source>
        <translation>Traceroute nie je nainštalovaný</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="282"/>
        <source>Traceroute is not installed, do you want to install it now?</source>
        <translation>Traceroute nie je nainštalovaný, chcete ho nainštalovať?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="290"/>
        <source>Traceroute hasn&apos;t been installed</source>
        <translation>Traceroute nebol nainštalovaný</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="291"/>
        <source>Traceroute cannot be installed. This may mean you are using the LiveCD or you are unable to reach the software repository,</source>
        <translation>Traceroute nejde nainštalovať. Možno používate LiveCD alebo nie je možné vytvoriť spojenie k softvérovým repozitárom. </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="304"/>
        <source>Traceroute is not installed and no Internet connection could be detected so it cannot be installed</source>
        <translation>Traceroute nie je nainštalovaný a zároveň nie ste pripojení k internetu, to znamená že program nepôjde nainštalovať</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="311"/>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>No destination host</source>
        <translation>Žiadny cieľový host</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Please fill in the destination host field</source>
        <translation>Prosím vyplňte políčko cieľový host</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="493"/>
        <source>Loaded Drivers</source>
        <translation>Používané ovládače</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="503"/>
        <source>Unloaded Drivers</source>
        <translation>Nepoužívané ovládače</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="519"/>
        <source>Blacklisted Drivers</source>
        <translation>Zakázané ovládače</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="542"/>
        <source>Blacklisted Broadcom Drivers</source>
        <translation>Zakázané bezdrátové ovládače</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1012"/>
        <source>enabled</source>
        <translation>Zapnuté</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1015"/>
        <source>disabled</source>
        <translation>Vypnúté</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1018"/>
        <source>WiFi hardware switch is off</source>
        <translation>Hardvérový WiFi spínač je vypnutý</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1026"/>
        <source>Locate the Windows driver you want to add</source>
        <translation>Určiť ovládač z Windows, ktorý chcete pridať</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1026"/>
        <source>Windows installation information file (*.inf)</source>
        <translation>Windows installation information file (*.inf)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>*.sys file not found</source>
        <translation>súbor *.sys nenájdený</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>The *.sys files must be in the same location as the *.inf file. %1 cannot be found</source>
        <translation>Súbory *.sys musia byť umiestnené v rovnakej  zložce ako súbor *.inf. %1 nejde nájsť</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1069"/>
        <source>sys file reference not found</source>
        <translation>referencia sys súboru nenájdená</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1069"/>
        <source>The sys file for the given driver cannot be determined after parsing the inf file</source>
        <translation>Po zanalyzování inf súboru nemôže byť priradený sys súbor pre daný ovládač</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1088"/>
        <source>Ndiswrapper driver removed.</source>
        <translation>Ovládač Ndiswrapper bol odstránený.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1096"/>
        <source>%1 Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1135"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1136"/>
        <source>Version: </source>
        <translation>Verzia:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1137"/>
        <source>Program for troubleshooting and configuring network for Garuda Linux</source>
        <translation>Program k riešeniu problémov a nastavenie bezdrátovej siete v systéme Garuda Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1139"/>
        <source>Copyright (c) MEPIS LLC and Garuda Linux</source>
        <translation>Copyright (c) MEPIS LLC a Garuda Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1140"/>
        <source>%1 License</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../about.cpp" line="32"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="33"/>
        <location filename="../about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>História zmien</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Zatvoriť</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="44"/>
        <source>You must run this program as root.</source>
        <translation>Tento program musíte spustiť ako užívateľ root.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="565"/>
        <source>Ndiswrapper is not installed</source>
        <translation>Ndiswrapper nie je nainštalovaný</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="577"/>
        <source>driver installed</source>
        <translation>nainštalovaný ovládač</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="588"/>
        <source> and in use by </source>
        <translation>a použivaný</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="597"/>
        <source>. Alternate driver: </source>
        <translation>. Alternatívny ovládač: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="689"/>
        <source>Driver removed from blacklist</source>
        <translation>Ovládač bol odstránený zo zoznamu zakázaných</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="690"/>
        <source>Driver removed from blacklist.</source>
        <translation>Ovládač bol odstránený zo zoznamu zakázaných.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="698"/>
        <location filename="../mainwindow.cpp" line="699"/>
        <source>Module blacklisted</source>
        <translation>Modul bol zakázaný</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="892"/>
        <source>Installation successful</source>
        <translation>Inštalácia prebehla úspešne</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="896"/>
        <source>Error detected, could not compile ndiswrapper driver.</source>
        <translation>Chyba, nebolo možné zkompilovať ovládač Ndiswrapper.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="901"/>
        <source>Error detected, could not install ndiswrapper.</source>
        <translation>Chyba, nebolo možné nainštalovať Ndiswrapper.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="913"/>
        <source>Error encountered while removing Ndiswrapper</source>
        <translation>V priebehu odstraňovania Ndiswrapper sa vyskyla chyba</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="971"/>
        <source>Unblacklist Driver</source>
        <translation>Odobrať ovládač zo zoznamu zakázaných</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="976"/>
        <source>Blacklist Driver</source>
        <translation>Pridať ovládač na zoznam zakázaných</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1118"/>
        <source>Could not unlock devices.
WiFi device(s) might already be unlocked.</source>
        <translation>Nepodarilo sa odblokovať zariadenia.
WiFi zadiadenie(a) už sú asi odblokované.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1120"/>
        <source>WiFi devices unlocked.</source>
        <translation>WiFi zariadenia boli odblokované.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1165"/>
        <location filename="../mainwindow.cpp" line="1166"/>
        <source>Driver loaded successfully</source>
        <translation>Ovládač bol úspešne načítaný</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1184"/>
        <location filename="../mainwindow.cpp" line="1185"/>
        <source>Driver unloaded successfully</source>
        <translation>Ovládač bol úspešne uvoľnený</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../mainwindow.cpp" line="718"/>
        <source>Could not load </source>
        <translation>Nejde načítať</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="754"/>
        <source>Could not unload </source>
        <translation>Nejde uvoľniť</translation>
    </message>
</context>
</TS>
