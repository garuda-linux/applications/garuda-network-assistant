<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hr">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="26"/>
        <source>Garuda Network Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="70"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="76"/>
        <source>IP address</source>
        <translation>IP adresa</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="112"/>
        <source>Hardware detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="143"/>
        <location filename="../mainwindow.ui" line="389"/>
        <location filename="../mainwindow.ui" line="502"/>
        <source>Re-scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="215"/>
        <source>Active interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="231"/>
        <source>WiFi status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="253"/>
        <source>Unblocks all soft/hard blocked wireless devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="256"/>
        <source>Unblock WiFi Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="288"/>
        <source>Linux drivers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="300"/>
        <source>Associated Linux drivers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="309"/>
        <source>Load Driver</source>
        <translation>Učitaj upravljački program</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="372"/>
        <source>Unload Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="409"/>
        <source>Blacklist Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="428"/>
        <source>Windows drivers</source>
        <translation>Windows upravljački programi</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="434"/>
        <source>Available Windows drivers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="468"/>
        <source>Remove Driver</source>
        <translation>Ukloni upravljački program</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="479"/>
        <source>Add Driver</source>
        <translation>Dodaj upravljački program</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="520"/>
        <source>About NDISwrapper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="532"/>
        <source>Install NDISwrapper</source>
        <translation>Instaliraj NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="543"/>
        <source>In order to use Windows drivers you need first to install NDISwrapper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="559"/>
        <source>Uninstall NDISwrapper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="591"/>
        <source>Net diagnostics</source>
        <translation>Dijagnostika mreže</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="603"/>
        <source>Ping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="609"/>
        <location filename="../mainwindow.ui" line="725"/>
        <source>Target URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="619"/>
        <source>Packets</source>
        <translation>Paketi</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="661"/>
        <location filename="../mainwindow.ui" line="780"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="678"/>
        <location filename="../mainwindow.ui" line="797"/>
        <source>Clear</source>
        <translation>Očisti</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="692"/>
        <location filename="../mainwindow.ui" line="811"/>
        <source>Cancel</source>
        <translation>Otkaži</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="719"/>
        <source>Traceroute</source>
        <translation>Traceroute</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="735"/>
        <source>Hops</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="866"/>
        <source>About...</source>
        <translation>O programu...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="889"/>
        <source>Help</source>
        <translation>Pomoć</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="957"/>
        <source>&amp;Close</source>
        <translation>&amp;Zatvori</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="964"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>IP address from router:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="88"/>
        <source>External IP address:</source>
        <translation>Vanjska IP adresa:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="211"/>
        <location filename="../mainwindow.cpp" line="225"/>
        <location filename="../mainwindow.cpp" line="239"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopiraj</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="213"/>
        <location filename="../mainwindow.cpp" line="227"/>
        <location filename="../mainwindow.cpp" line="241"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="214"/>
        <location filename="../mainwindow.cpp" line="228"/>
        <location filename="../mainwindow.cpp" line="242"/>
        <source>Copy &amp;All</source>
        <translation>Kopiraj &amp;sve</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="216"/>
        <location filename="../mainwindow.cpp" line="230"/>
        <location filename="../mainwindow.cpp" line="244"/>
        <source>Ctrl+A</source>
        <translation>CTRL+A</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="281"/>
        <location filename="../mainwindow.cpp" line="303"/>
        <source>Traceroute not installed</source>
        <translation>Traceroute nije instaliran</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="282"/>
        <source>Traceroute is not installed, do you want to install it now?</source>
        <translation>Traceroute nije instaliran, želite li ga instalirati sada?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="290"/>
        <source>Traceroute hasn&apos;t been installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="291"/>
        <source>Traceroute cannot be installed. This may mean you are using the LiveCD or you are unable to reach the software repository,</source>
        <translation>Traceroute se ne može instalirati. To može značiti da koristite LiveCD ili da niste u mogućnosti dohvatiti repozitorije softwarea.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="304"/>
        <source>Traceroute is not installed and no Internet connection could be detected so it cannot be installed</source>
        <translation>Traceroute nije instaliran i nema otkrivene internetske veze te ne može biti instaliran</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="311"/>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>No destination host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Please fill in the destination host field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="493"/>
        <source>Loaded Drivers</source>
        <translation>Učitani upravljački programi</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="503"/>
        <source>Unloaded Drivers</source>
        <translation>Neučitani upravljački programi</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="519"/>
        <source>Blacklisted Drivers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="542"/>
        <source>Blacklisted Broadcom Drivers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1012"/>
        <source>enabled</source>
        <translation>omogućeno</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1015"/>
        <source>disabled</source>
        <translation>onemogućeno</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1018"/>
        <source>WiFi hardware switch is off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1026"/>
        <source>Locate the Windows driver you want to add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1026"/>
        <source>Windows installation information file (*.inf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>*.sys file not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>The *.sys files must be in the same location as the *.inf file. %1 cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1069"/>
        <source>sys file reference not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1069"/>
        <source>The sys file for the given driver cannot be determined after parsing the inf file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1088"/>
        <source>Ndiswrapper driver removed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1096"/>
        <source>%1 Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1135"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1136"/>
        <source>Version: </source>
        <translation>Inačica:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1137"/>
        <source>Program for troubleshooting and configuring network for Garuda Linux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1139"/>
        <source>Copyright (c) MEPIS LLC and Garuda Linux</source>
        <translation>Autorska prava (c) MEPIS LLC and Garuda Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1140"/>
        <source>%1 License</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../about.cpp" line="32"/>
        <source>License</source>
        <translation>Licenca</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="33"/>
        <location filename="../about.cpp" line="43"/>
        <source>Changelog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Otkaži</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Zatvori</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="44"/>
        <source>You must run this program as root.</source>
        <translation>Morate pokrenuti ovaj program kao root.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="565"/>
        <source>Ndiswrapper is not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="577"/>
        <source>driver installed</source>
        <translation>upravljački program je instaliran</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="588"/>
        <source> and in use by </source>
        <translation>i koristi ga</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="597"/>
        <source>. Alternate driver: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="689"/>
        <source>Driver removed from blacklist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="690"/>
        <source>Driver removed from blacklist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="698"/>
        <location filename="../mainwindow.cpp" line="699"/>
        <source>Module blacklisted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="892"/>
        <source>Installation successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="896"/>
        <source>Error detected, could not compile ndiswrapper driver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="901"/>
        <source>Error detected, could not install ndiswrapper.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="913"/>
        <source>Error encountered while removing Ndiswrapper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="971"/>
        <source>Unblacklist Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="976"/>
        <source>Blacklist Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1118"/>
        <source>Could not unlock devices.
WiFi device(s) might already be unlocked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1120"/>
        <source>WiFi devices unlocked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1165"/>
        <location filename="../mainwindow.cpp" line="1166"/>
        <source>Driver loaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1184"/>
        <location filename="../mainwindow.cpp" line="1185"/>
        <source>Driver unloaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../mainwindow.cpp" line="718"/>
        <source>Could not load </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="754"/>
        <source>Could not unload </source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
