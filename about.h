#ifndef ABOUT_H
#define ABOUT_H

#include <QString>
#include <cmd.h>

void displayDoc(QString url, QString title, QWidget* parent, bool modal=false);
void displayAboutMsgBox(QString title, QString message, QString licence_url, QString license_title, QWidget* parent);

#endif // ABOUT_H
