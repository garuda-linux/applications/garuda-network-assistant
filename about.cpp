#include "about.h"
#include "cmd.h"
#include "version.h"

#include <QApplication>
#include <QFileInfo>
#include <QMessageBox>
#include <QPushButton>
#include <QTextBrowser>
#include <QTextEdit>
#include <QVBoxLayout>

void displayDoc(QString url, QString title, QWidget* parent, bool modal)
{
    QDialog *doc = new QDialog(parent);
    doc->setWindowTitle(title);
    doc->resize(1280, 720);

    QTextBrowser *browser = new QTextBrowser;
    browser->setOpenLinks(false);
    browser->setSource(QUrl(url));

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(browser);
    doc->setLayout(layout);
    if(modal) {
        doc->exec();
        delete doc;
    } else {
        doc->show();
    }
}

void displayAboutMsgBox(QString title, QString message, QString licence_url, QString license_title, QWidget* parent)
{
    QMessageBox msgBox(QMessageBox::NoIcon, title, message, QMessageBox::NoButton, parent);
    QPushButton *btnLicense = msgBox.addButton(QApplication::tr("License"), QMessageBox::HelpRole);
    QPushButton *btnChangelog = msgBox.addButton(QApplication::tr("Changelog"), QMessageBox::HelpRole);
    QPushButton *btnCancel = msgBox.addButton(QApplication::tr("Cancel"), QMessageBox::NoRole);
    btnCancel->setIcon(QIcon::fromTheme("window-close"));

    msgBox.exec();

    if (msgBox.clickedButton() == btnLicense) {
        displayDoc(licence_url, license_title, &msgBox, true);
    } else if (msgBox.clickedButton() == btnChangelog) {
        QDialog *changelog = new QDialog();
        changelog->setWindowTitle(QApplication::tr("Changelog"));
        changelog->resize(600, 500);

        QTextEdit *text = new QTextEdit;
        text->setReadOnly(true);
        Cmd cmd;
        text->setText(cmd.getCmdOut("zless /usr/share/doc/" + QFileInfo(QCoreApplication::applicationFilePath()).fileName()  + "/changelog.gz"));

        QPushButton *btnClose = new QPushButton(QApplication::tr("&Close"));
        btnClose->setIcon(QIcon::fromTheme("window-close"));
        QApplication::connect(btnClose, &QPushButton::clicked, changelog, &QDialog::close);

        QVBoxLayout *layout = new QVBoxLayout;
        layout->addWidget(text);
        layout->addWidget(btnClose);
        changelog->setLayout(layout);
        changelog->exec();
        delete changelog;
    }
}
